FROM finalduty/archlinux:monthly

MAINTAINER Kevin Pouget <kpouget@imag.fr>

ENTRYPOINT ["bash"]
CMD ["/assets_dema/init.sh"]

RUN mkdir -p /home/dema
RUN echo mcgdb.dema.docker.local > /etc/hostname

WORKDIR /home/dema

RUN echo "## Allow all users to run any commands anywhere" >> /etc/sudoers
RUN echo "ALL ALL= NOPASSWD:	ALL" >> /etc/sudoers
RUN echo "auth    sufficient    pam_permit.so" > /etc/pam.d/su

RUN pacman --noconfirm --needed -Su
RUN pacman --noconfirm --needed -Sy gdb strace libdwarf make binutils gcc vim sudo python-pip git openssh \
    && rm -rf /var/cache/pacman/pkg/ \
    && mkdir -p /var/cache/pacman/pkg/

# End of system preparation
# Beginning of mcgdb/dema preparation

RUN pip install pysigset enum34 pyparsing colorlog

# perf: mcgdb performance debugger
# cmake clang: Intel OpenMP runtime
RUN pacman --noconfirm --needed -Sy perf cmake clang wget

# mcgdb internal profiling
RUN pip install profilehooks

# seqdiag
RUN pip install funcparserlib
RUN pacman --noconfirm --needed -Sy python-pillow

# temanejo
RUN pacman --noconfirm --needed -Sy mesa-libgl \
        python-pyside-tools swig graphviz  python2-pip  \
        pkg-config automake autoconf
 
RUN pip2 install networkx pygraphviz 
RUN pip2 install pyside

# seqdiag
RUN pip install packaging six appdirs webcolors funcparserlib olefile

# temanejo
RUN pacman --noconfirm  -Sy gperf ruby bison flex patch

# mcgdb documentation
RUN pip3 install sphinx pyparsing

# End of mcgdb/dema preparation

COPY assets /assets_dema

RUN date > /assets_dema/version
COPY Dockerfile /assets_dema/

VOLUME /home/dema
