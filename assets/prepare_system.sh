#! /bin/sh

# cleanup files copied from /assets
rm -f ~/Dockerfile ~/init.sh

mkdir -p  ~/.ssh
[[ -e ssh_known_hosts ]] && mv ssh_known_hosts .ssh/known_hosts

mkdir -p ~/python ~/tools ~/bin


