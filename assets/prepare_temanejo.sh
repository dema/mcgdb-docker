#! /bin/bash

source ./prepare_config.sh

##########################

cat > $HOME/bin/pdflatex <<EOF
#!/bin/sh
touch \$(basename "\$1" .tex).pdf
EOF
chmod u+x $HOME/bin/pdflatex

cd tools
git_clone_or_pull $DEMA_GIT_ROOT/mcgdb-temanejo.git end-of-dema

cd mcgdb-temanejo

mkdir -p $TEMANEJO_INSTALL

[[ ! -e ./configure ]] && autoreconf -fiv
[[ ! -e ./Makefile ]] && ./configure --prefix=$TEMANEJO_INSTALL

if [[ ! -e ./Makefile ]]
then
    echo "Could not generate Temanejo Makefile, something went wrong ..."
    exit 1
fi

make 

pushd .
cd $TEMANEJO_HOME/Temanejo2/src/temanejo2/temanejo2/view
pyside-uic mainwindow.ui -o mainwindow.py
popd


make install 

rm -f $HOME/bin/pdflatex
sed '1s/env python$/env python2/' $TEMANEJO_INSTALL/bin/Temanejo2 -i
ln -fs $TEMANEJO_INSTALL/bin/Temanejo2 $HOME/bin/Temanejo2

cat <<EOF
Done :)


To run: Temanejo2 (already in the PATH) or $TEMANEJO_INSTALL/bin/Temanejo2

Already in set in prepare_env.sh:
# export PYTHONPATH=\$PYTHONPATH:$TEMANEJO_INSTALL/lib/python2.7/site-packages/
PYTHONPATH=$PYTHONPATH
EOF

