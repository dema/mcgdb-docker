#! /bin/bash

source ./prepare_config.sh


cd $HOME/python
git_clone_or_pull $DEMA_GIT_ROOT/mcgdb.git end-of-dema

cd $HOME/python/mcgdb
make -C model/task/environment/openmp/capture/preload
make -C model/profiling/
make -C model/numa/

ln -fs $(which gdb) $HOME/bin/mcgdb
ln -fs $(which gdb) $HOME/bin/mcgdb-omp
