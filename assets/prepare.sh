#! /bin/bash

./prepare_system.sh

./prepare_mcgdb.sh
./prepare_mcgdb_documentation.sh

./prepare_intel_omp.sh
./prepare_seqdiag.sh
./prepare_temanejo.sh

./prepare_sample.sh

