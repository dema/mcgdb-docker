#!/bin/bash

cat <<EOF
##########
#
# DEMA/mcgdb environment
#
##########
EOF
##########


##### ENVIRONMENT #####
USER_NAME=dema

ASSETS=/assets_${USER_NAME}/
ASSETS_HOME=$ASSETS/home/

HOME=/home/$USER_NAME
IMAGE_NAME=kpouget/dema-mcgdb

##### admin stuff #####


echo "##########"
echo "# Running an Archlinux container"
echo "# Docker image built on $(cat $ASSETS/version)"
echo "##########"
echo

GROUPID=$(stat $HOME -c %g)
USERID=$(stat $HOME -c %u)

if [[ $USERID -eq 0 && $GROUPID -eq 0 ]]
then

    echo "####################"
    echo "WARNING: Directory '$HOME' not shared with host, is it on purpose ? if not, please use this command line:"
    echo "    HOME_DIR=\$HOME/${USER_NAME}_data; mkdir -p \$HOME_DIR"
    echo "    docker run --rm -it -v \$HOME_DIR:/home/$USER_NAME  --cap-add sys_ptrace $IMAGE_NAME"
    echo "####################"
    echo
    GROUPID=1000
    USERID=1000
    NO_HOME=1
fi

echo "root:root" | chpasswd 

groupadd --gid $GROUPID $USER_NAME --non-unique
useradd --uid $USERID --gid $GROUPID $USER_NAME
echo $USER_NAME:$USER_NAME | chpasswd

chown $USER_NAME:$USER_NAME /home/$USER_NAME

echo "==++++++++++++++++++++++++++="
echo "== Root password is 'root'"
echo "== User ($USER_NAME) is '$USER_NAME'" 
echo "==++++++++++++++++++++++++++="

##### check strace #####

strace ls &>/dev/null
if [ $? -eq 1 ] 
then
    echo "####################"
    echo 'ERROR: ptrace not working. Did you pass --privileged or --cap-add sys_ptrace option to docker ?'
    echo "####################"
    echo
fi

##### prepare home dir #####

if [ -z "$(ls -A $HOME/ 2>/dev/null)" ]
then
    echo "INFO: Home dir empty, we'll prepare it."
    HOME_NOT_POPULATED=1
fi

if [ "$HOME_NOT_POPULATED" == "1" ]
then
    echo "INFO: Copying asset files to $HOME ..."
    cp $ASSETS_HOME/. $HOME -ra
    echo "INFO: Giving user rights to $HOME ..."
    chown -R $USER_NAME:$USER_NAME $HOME
fi
cp $ASSETS/prepare* $HOME
chown -R $USER_NAME:$USER_NAME $HOME/prepare*

echo "INFO: Ready! Read ~/README.md for more details."
echo

exec su $USER_NAME
