#! /bin/bash

make -C $HOME/sample/tasks
make -C $HOME/sample/regions

cd ~/sample

NPB_REPO=NPB3.0-omp-C

if [ ! -d $NPB_REPO ]; then
    # commit https://github.com/benchmark-subsetting/NPB3.0-omp-C/commit/5d565d916cad47bf09a2fa7432bf8e5fa95446ec
    git clone https://github.com/benchmark-subsetting/$NPB_REPO.git
fi

cd $NPB_REPO

cp /assets_dema/${NPB_REPO}__make.def config/make.def
cp /assets_dema/${NPB_REPO}__suite.def config/suite.def

mkdir -p bin

make suite
