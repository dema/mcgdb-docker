#! /bin/bash

DEMA_GIT_ROOT=https://gitlab.inria.fr/dema

GLOBAL_CLONE_OPT="--depth 1"

git_clone_or_pull() {
    URL=$1
    BRANCH=$2 # or tag

    REPO_NAME=$(basename $URL .git)
    
    if [ ! -d $REPO_NAME ] # repo already cloned
    then
        if [[ ! -z "$BRANCH" ]]; then
            BRANCH_OPT="--branch end-of-dema"
        fi
        git clone $URL $BRANCH_OPT $GLOBAL_CLONE_OPT
    else
        if [[ ! -z "$BRANCH" ]]; then
            BRANCH_OPT="origin $BRANCH"
        fi
        pushd .
        cd $REPO_NAME
        git pull $BRANCH_OPT
        popd
    fi
    
}

TEMANEJO_HOME=$HOME/tools/mcgdb-temanejo
TEMANEJO_INSTALL=$TEMANEJO_HOME/install
