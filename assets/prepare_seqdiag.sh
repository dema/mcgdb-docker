#! /bin/bash

cd $HOME/tools
git_clone_or_pull $DEMA_GIT_ROOT/mcgdb-seqdiag.git end-of-dema

ln -fs $HOME/tools/mcgdb-seqdiag/seqdiag.py $HOME/bin/seqdiag
