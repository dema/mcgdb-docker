#! /bin/bash

INTEL_OMP_HOME=$HOME/tools/intel_omp/

# url checked 06/02/2017
INTEL_OMP_URL=https://www.openmprtl.org/sites/default/files/libomp_20150701_oss.tgz

source ./prepare_config.sh

##########################

mkdir -p $INTEL_OMP_HOME/{build,install}

cd $INTEL_OMP_HOME

[ ! -e $(basename $INTEL_OMP_URL) ] && wget $INTEL_OMP_URL
[ ! -e  libomp_oss ] && tar xf $(basename $INTEL_OMP_URL)

cp libomp_oss/src/include/41/ompt.h.var libomp_oss/src/ompt.h # I don't know why ...

cd build

[ -e Makefile ] && make clean

cmake -DCMAKE_C_FLAGS="-g -O0" \
      -DCMAKE_INSTALL_PREFIX:PATH=$INTEL_OMP_HOME/install \
      -DLIBOMP_OMPT_SUPPORT=true \
      $INTEL_OMP_HOME/libomp_oss/

make && make install

ln -fs libiomp5.so $HOME/tools/intel_omp/install/lib/libomp.so

## already in prepare_env.sh:
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$HOME/tools/intel_omp/install/lib/

