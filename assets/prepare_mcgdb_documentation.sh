#! /bin/bash

cd ~/python/mcgdb/documentation || echo "Could not find mcgdb documentation dir ..."

make html WITH_VIEW_CODE=1

echo "TO DISTRIBUTE: upload content of $(pwd)/_sources/html"
