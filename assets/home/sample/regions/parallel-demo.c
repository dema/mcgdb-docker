#include <omp.h>
#include <stdio.h>
int main() {
  printf("Beginning of main\n");

#pragma omp parallel
  {
    int id = omp_get_thread_num() + 1;

#pragma omp single
    printf("Single hello\n");
    printf("@%d in the parallel zone\n", id);

#pragma omp critical
    {
      printf("----------\n");
      printf("@%d in critical zone\n", id);
      printf("----------\n");
    }

#pragma omp sections
    {
#pragma omp section
      {
        printf("@%d in section 1!\n", id);
      }
#pragma omp section
      {
        printf("@%d in section 2!\n", id);
      }
#pragma omp section
      {
        printf("@%d in section 3!\n", id);
      }
    }

#pragma omp barrier
    
  printf("End of main!\n");
  }
}
