#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdint.h>



//#pragma omp task depend(inout: *i)
void foo ( int *i ){
    printf("Start foo(): %ld.\n",(uint64_t)  pthread_self());
};



void foo1 ( int *i, int *j, int *k ){
  printf("Start foo1(): %ld.\n",(uint64_t)  pthread_self());
};


int main (void)
{
  int i=0;
    int j=0;
    int k=0;
    int x=0;
    int y=0;
    int z=0;
#pragma omp parallel
    {
#pragma omp single
      {
#pragma omp task depend(in: i,j,k) depend(out:i,j,k)
        foo1(&i, &j, &k);
#pragma omp task depend(in:x,y,z) depend(out:x,y,z)
        foo1(&x, &y, &z);
#pragma omp task depend(in:i,y,z) depend(out:i,y,z)
        foo1(&i, &y, &z);
#pragma omp task depend(in:x,j,k) depend(out:x,j,k)
        foo1(&x, &j, &k);
        
        for ( i = 0; i < 2; ++i ){
#pragma omp task depend(in: i) depend(out: i)
          foo(&i);
#pragma omp task depend(in: j) depend(out: j)
          foo(&j);
#pragma omp task depend(in: k) depend(out: k)
          foo(&k);
#pragma omp task depend(in: x) depend(out: x)
          foo(&x);
#pragma omp task depend(in: y) depend(out: y)
          foo(&y);
#pragma omp task depend(in: z) depend(out: z)
          foo(&z);
        }
#pragma omp task depend(in:i,j,k) depend(out:i,j,k)
        foo1(&i, &j, &k);
#pragma omp task depend(in:x,y,z) depend(out:x,y,z)
        foo1(&x, &y, &z);
#pragma omp task depend(in:i,y,z) depend(out:i,y,z)
        foo1(&i, &y, &z);
#pragma omp task depend(in: x,j,k) depend(out: x,j,k)
        foo1(&x, &j, &k);
      }
    }
#pragma omp taskwait
    
    
    return 0;
}
