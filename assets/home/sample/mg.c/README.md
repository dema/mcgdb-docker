(from the mg.c debugging paper)

(don't run mg.c on a normal machine, it will eat all your memory!)

# Attesting the Performance Problem

    (gdb) mcgdb load_model_by_name omp
    (gdb) tbreak resid
    (gdb) run
    ...
    (gdb) omp loop profile iterations
    (gdb) set profile-perf-counters   \
                           instructions
    (gdb) continue
    ... ^C
    (gdb) profile graph plot-all all
    (gdb) profile graph offline
    numa nore | .......  <
    omp_loop_len | ....
    instructions | .... y2


## Pinpointing the NUMA data-access problem

    (gdb) omp loop profile iterations
    (gdb) set profile-perf-counters  \
                   instructions,cycles
    (gdb) continue
    ... ^C
    (gdb) profile graph plot-all all
    (gdb) profile graph offline
    numa node | ....... y# <
    instructions | .... /
    cycles | ..........

# Finding the Performance Problem

    ./to_debug -ex "source to_measure_pagefaults_normal"
    ./to_debug -ex "source to_measure_pagefaults_no_touch"

# Fixing the problem

    ./to_debug -ex "source to_spread_heap"

    ./to_debug -ex "source to_notouch_malloc"
