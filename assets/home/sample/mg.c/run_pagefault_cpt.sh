#! /bin/bash

source env

source cfg_malloc
./to_debug -ex "source gdb_measure_pagefaults"

source cfg_malloc_notouch
./to_debug -ex "source gdb_measure_pagefaults"


cat <<EOF

Allocator configuration
  Alignment: 64
  Interleaving: NO
  Use huge pages: NO
  Touch blocks: YES

AFTER ALLOCATION
| min_flt:                     873,606

AFTER INITALISATION
| min_flt:                      15,649


Allocator configuration
  Alignment: 64
  Interleaving: NO
  Use huge pages: NO
  Touch blocks: NO

AFTER ALLOCATION
| min_flt:                       4,470

AFTER INITALISATION
| min_flt:                     885,123


EOF >> /dev/null
