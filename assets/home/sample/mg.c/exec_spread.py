AFTER_ALLOC_BPT_LOC = "mg.c:287" # printf(" Benchmark completed\n");

SILENT_REMAP = False

class SpreadBreakpoint (gdb.Breakpoint):
  def __init__(self):
    gdb.Breakpoint.__init__(self, AFTER_ALLOC_BPT_LOC, temporary=True)


  def stop (self):
      print("SPREADING ...")

      gdb.execute("numa spread_pages 1", to_string=SILENT_REMAP)

      print("SPREADING DONE.")

      return False

SpreadBreakpoint()
gdb.execute("run")
