REMAP_V = True
REMAP_R = [5, 10]
REMAP_U = REMAP_R

AFTER_ALLOC_BPT_LOC = "mg.c:287" # printf(" Benchmark completed\n");

SILENT_REMAP = True

class RemapBreakpoint (gdb.Breakpoint):
  def __init__(self):
    gdb.Breakpoint.__init__(self, AFTER_ALLOC_BPT_LOC, temporary=True)
  
  def stop (self):
    print("REMAPPING ...")
    
    if REMAP_V:
      print("Remapping v ...")
      gdb.execute("numa move_3D_matrix_spread v m3[{i}] m2[{i}] m1[{i}] sizeof(double)".format(i=9), to_string=SILENT_REMAP)

    def do_matrix_array_remap(name, remap_range):
      if not remap_range:
        return
      
      print("Remapping {}[{}-{}] ...".format(name, remap_range[0], remap_range[1]-1))

      for i in range(*remap_range):
        gdb.execute("numa move_3D_matrix_spread {name}[{i}] m3[{i}] m2[{i}] m1[{i}] sizeof(double)".format(name=name, i=i), to_string=SILENT_REMAP)
            
    do_matrix_array_remap("r", REMAP_R)
    do_matrix_array_remap("u", REMAP_U)
    
    gdb.execute("numa pages_moved")
    print("REMAPPING DONE.")
    
    return False
  
RemapBreakpoint()
gdb.execute("run")
