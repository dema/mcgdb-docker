
class MyBreakpoint (gdb.Breakpoint):
  def __init__(self):
    # before printf(" Benchmark completed\n");
    gdb.Breakpoint.__init__(self, "mg.c:287", temporary=True)
  
  def stop (self):
    print("REMAPPING ...")

    i = 9
    gdb.execute("numa move_3D_matrix_spread v m3[{i}] m2[{i}] m1[{i}] sizeof(double)".format(i=i))

    for i in range(5, 10):
      gdb.execute("numa move_3D_matrix_spread r[{i}] m3[{i}] m2[{i}] m1[{i}] sizeof(double)".format(i=i))
      gdb.execute("numa move_3D_matrix_spread u[{i}] m3[{i}] m2[{i}] m1[{i}] sizeof(double)".format(i=i))
      pass
    gdb.execute("numa pages_moved")
    print("REMAPPING DONE.")
    
    return False
  
MyBreakpoint()
gdb.execute("run")
