## WARNING: use this only in safe environments !
# this will automatically load <objfile>-gdb.py (eg: /home/dema/sample/tasks/minimal_omp_threads-gdb.py)

set auto-load safe-path /

## no window size
set height 0
set width 0

## CLI convenience
set breakpoint pending on
set print pretty
set confirm off

set print pretty
set history filename ~/.gdb_history
set history save

# for docker
set disable-randomization off

## python debugging
set python print-stack full

# New commands for Python:
# (gdb) pp py_obj
# <> print(str(py_obj)) ...
#
# (gdb) ppd py_obj
# <> print(dir(py_obj))

python
import gdb

class pp(gdb.Command):
        """Python print its args"""
        def __init__(self):
                gdb.Command.__init__ (self, "pp", gdb.COMMAND_DATA, completer_class=gdb.COMPLETE_SYMBOL)

        def invoke (self, arg, from_tty):
                gdb.execute("python(print(%s))" % arg) # do it in GDB python env, not here
pp()

class ppd(gdb.Command):
        """Python print dir() of its args"""
        def __init__(self):
                gdb.Command.__init__ (self, "ppd", gdb.COMMAND_DATA, completer_class=gdb.COMPLETE_SYMBOL)

        def invoke (self, arg, from_tty):
                gdb.execute("python(print(dir(%s)))" % arg) # do it in GDB python env, not here
ppd()
end


##
## Load mcgdb
##

python
sys.path.append("/home/dema/python")
try:
  import mcgdb
  #mcgdb.initialize()
  mcgdb.initialize_by_name()
except Exception as e:
  import traceback
  print ("Couldn’t load mcGDB extension: {}".format(e))
  traceback.print_exc()
end
