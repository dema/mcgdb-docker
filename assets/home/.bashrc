export PS1="\u@\w \\$ \[$(tput sgr0)\]"

alias ls="ls --color"
alias la="ls -a"
alias llh="ls -lh"
alias vi="vim"

[[ -f ~/.profile ]] && source .profile

export PATH=$PATH:$HOME/bin

source ~/prepare_env.sh
